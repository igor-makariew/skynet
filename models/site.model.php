<?php
    require_once ('core/model.php');

    class SiteModel extends Model{

        function registration_user($login, $email, $password, $status){
            $password_hash = password_hash($password, PASSWORD_DEFAULT);
            $query = "INSERT INTO `registration` (`login`, `email`, `password`, `status`) VALUES ('{$login}', '{$email}', '{$password_hash}', '{$status}')";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function select_email_user($email){
            $query = "SELECT * FROM `registration` WHERE `email` = '{$email}'";
            $result = mysqli_query($this->get_link(), $query) or  die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_assoc($result);
            if(!isset($result_array['email'])){
                return true;
            }else{
                return false;
            }
        }

        function verify_pasword_user($login, $password){
            $query = "SELECT * FROM `registration` WHERE `login` = '{$login}'";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_assoc($result);
            if(password_verify($password, $result_array['password'])){
                return true;
            }else{
                return false;
            }
        }

        function select_user($login){
            if(isset($login)){
                $query = "SELECT * FROM `registration` WHERE `login` = '{$login}'";
                $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
                $result_array = mysqli_fetch_assoc($result);
                return $result_array;
            } else{
                return GUEST;
            }
        }

        function get_url_params($url){
            $url_params = explode('&', $url);
            $sort_param = explode('=', $url_params[1]);
            return $sort_param[0];
        }

        function get_url_value($url_value){
            return $_GET[$url_value];
        }

        function get_order_by_column($value_column){
            $sort_column = explode('_', $value_column);
            return $sort_column[1];
        }

        function sort_tasks($column, $get_value){
            $query = "SELECT * FROM `tasks` ORDER BY {$column} {$get_value}";
            $result = mysqli_query($this->get_link(),$query) or die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $result_array;
        }





    }
