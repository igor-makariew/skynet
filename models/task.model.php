<?php
    require_once ('core/model.php');


    class TaskModel extends Model {

        function set_task_user($login, $email, $status, $text_task){
            $status = $this->role_user($status);

            $query = "INSERT INTO `tasks` (`login`, `email`, `status`, `text_task`) VALUES ('{$login}', '{$email}', '{$status}', '{$text_task}')";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }

        function role_user($role_user){
            $array_role_user = ['GUEST' => 10, 'USER' => 5, 'MODERATOR' => 3, 'ADMIN' => 2, 'SUPERADMIN' => 1];
            $key_array = '';
            foreach ($array_role_user as $key => $value){
                if($role_user == $value){
                    $key_array = $key;
                }
            }
            return $key_array;
        }

        function get_tasks(){
            $query = "SELECT * FROM `tasks`";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $result_array;
        }

        function sort_login(){
            $query = "SELECT * FROM `tasks` ORDER BY `login` ASC";
            $result = mysqli_query($this->get_link(),$query) or die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $result_array;
        }

        function get_count_tasks(){
            $query = "SELECT count(*) FROM `tasks`";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $result_array;
        }

        function array_params_sort($params_sort){
            $array_params = ['sort_login', 'sort_email', 'sort_status'];
            foreach ($array_params as $value){
                if($value == $params_sort){
                    return true;
                }
            }
            return false;
        }

        function update_tasks($number_task, $text_task){
            $query = "UPDATE `tasks` SET `text_task` = '{$text_task}', `moderation` = 1 WHERE `id` = '{$number_task}'";
            $result = mysqli_query($this->get_link(), $query) or die(mysqli_error($this->get_link()));
            if($result){
                return true;
            }else{
                return false;
            }
        }
    }