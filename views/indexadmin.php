<div class="site-section testimonial-wrap">
    <div class="container">

        <form method="post">
            <div class="form-group">
                <label for="exampleFormControlLogin">№ task</label>
                <input type="text" class="form-control" id="exampleFormControlLogin" placeholder="№" name="task">
            </div>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Text task</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="text" name="text_task"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>

    </div>
</div>

<div class="site-section testimonial-wrap">
    <div class="container">

        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">№</th>
                <th scope="col"><a href="?action=index&sort_login=<?= ($_GET['sort_login'] == 'ASC')?'DESC':'ASC'?>&page=<?= $_SESSION['page'] ?>">Login</a></th>
                <th scope="col"><a href="?action=index&sort_email=<?= ($_GET['sort_email'] == 'ASC')?'DESC':'ASC'?>&page=<?= $_SESSION['page'] ?>">Email</a></th>
                <th scope="col"><a href="?action=index&sort_status=<?= ($_GET['sort_status'] == 'ASC')?'DESC':'ASC'?>&page=<?= $_SESSION['page'] ?>">Status</a></th>
                <th scope="col">Tasks</th>
                <th scope="col">Moderation</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($params['table'] as $value):?>
                <tr>
                    <th scope="row"><?= $value['id'] ?></th>
                    <td><?= $value['login'] ?></td>
                    <td><?= $value['email'] ?></td>
                    <td><?= $value['status'] ?></td>
                    <td><?= $value['text_task'] ?></td>
                    <td><?= $value['moderation'] ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

    </div>
</div>

<div class="row mb-5 justify-content-center text-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="/?action=admin/indexadmin&page=1">Previous</a></li>
            <?php for ($i = 1; $i <= $params['pages']; $i++):?>
                <li class="page-item <?= $_SESSION['page'] == $i?'active':''?>"><a class="page-link" href="/?action=admin/indexadmin&page=<?= $i ?>"><?= $i ?></a></li>
            <?php endfor;?>
            <li class="page-item"><a class="page-link" href="/?action=admin/indexadmin&page=<?= $params['pages'] ?>">Last</a></li>
        </ul>
    </nav>
</div>
