<div class="site-section testimonial-wrap">
    <div class="container">

        <form method="post">
            <div class="form-group">
                <label for="exampleFormControlLogin">Login</label>
                <input type="text" class="form-control" id="exampleFormControlLogin" placeholder="login" name="login">
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Email address</label>
                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" name="email">
            </div>

            <div class="form-group">
                <input type="hidden" class="form-control" id="exampleFormControlStatus" value="<?= $_SESSION['user']['login']?$_SESSION['user']['status']:GUEST?>" name="status">
            </div>

            <div class="form-group">
                <label for="exampleFormControlTextarea1">Text task</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="text" name="text_task"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Tasks</button>
        </form>

    </div>
</div>

<div class="site-section testimonial-wrap">
    <div class="container">

        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">№</th>
                    <th scope="col"><a href="?action=index&sort_login=<?= ($_GET['sort_login'] == 'ASC')?'DESC':'ASC'?>&page=<?= $_SESSION['page'] ?>">Login</a></th>
                    <th scope="col"><a href="?action=index&sort_email=<?= ($_GET['sort_email'] == 'ASC')?'DESC':'ASC'?>&page=<?= $_SESSION['page'] ?>">Email</a></th>
                    <th scope="col"><a href="?action=index&sort_status=<?= ($_GET['sort_status'] == 'ASC')?'DESC':'ASC'?>&page=<?= $_SESSION['page'] ?>">Status</a></th>
                    <th scope="col">Tasks</th>
                    <th scope="col">Moderation</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($params['table'] as $value):?>
                <tr>
                    <th scope="row"><?= $value['id'] ?></th>
                    <td><?= $value['login'] ?></td>
                    <td><?= $value['email'] ?></td>
                    <td><?= $value['status'] ?></td>
                    <td><?= $value['text_task'] ?></td>
                    <td><input type="checkbox" <?= ($value['moderation'] == 1)?'checked = "checked"':'' ?>disabled="disabled" > </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>

    </div>
</div>

<div class="row mb-5 justify-content-center text-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="/?action=index&page=1">Previous</a></li>
        <?php for ($i = 1; $i <= $params['pages']; $i++):?>
            <li class="page-item <?= $_SESSION['page'] == $i?'active':''?>"><a class="page-link" href="/?action=index&page=<?= $i ?>"><?= $i ?></a></li>
        <?php endfor;?>
            <li class="page-item"><a class="page-link" href="/?action=index&page=<?= $params['pages'] ?>">Last</a></li>
        </ul>
    </nav>
</div>


<!--<div class="site-section services-1-wrap">-->
<!--    <div class="container">-->
<!--        <div class="row mb-5 justify-content-center text-center">-->
<!--            <div class="col-lg-5">-->
<!--                <h3 class="section-subtitle">What We Do</h3>-->
<!--                <h2 class="section-title mb-4 text-black">We Are <strong>Leading Industry</strong> of Engineering. We Love What We Do</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row no-gutters">-->
<!--            <div class="col-lg-3 col-md-6">-->
<!--                <div class="service-1">-->
<!--                    <span class="number">01</span>-->
<!--                    <div class="service-1-icon">-->
<!--                        <span class="flaticon-engineer"></span>-->
<!--                    </div>-->
<!--                    <div class="service-1-content">-->
<!--                        <h3 class="service-heading">Professional Team</h3>-->
<!--                        <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-3 col-md-6">-->
<!--                <div class="service-1">-->
<!--                    <span class="number">02</span>-->
<!--                    <div class="service-1-icon">-->
<!--                        <span class="flaticon-compass"></span>-->
<!--                    </div>-->
<!--                    <div class="service-1-content">-->
<!--                        <h3 class="service-heading">Great Ideas</h3>-->
<!--                        <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-3 col-md-6">-->
<!--                <div class="service-1">-->
<!--                    <span class="number">03</span>-->
<!--                    <div class="service-1-icon">-->
<!--                        <span class="flaticon-oil-platform"></span>-->
<!--                    </div>-->
<!--                    <div class="service-1-content">-->
<!--                        <h3 class="service-heading">Quality Building</h3>-->
<!--                        <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-3 col-md-6">-->
<!--                <div class="service-1">-->
<!--                    <span class="number">04</span>-->
<!--                    <div class="service-1-icon">-->
<!--                        <span class="flaticon-crane"></span>-->
<!--                    </div>-->
<!--                    <div class="service-1-content">-->
<!--                        <h3 class="service-heading">Quality Works</h3>-->
<!--                        <p>Gravida sodales condimentum pellen tesq accumsan orci quam sagittis sapie</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--END services-->
<!---->
<!--<div class="site-section">-->
<!--    <div class="block-2">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-6 mb-4 mb-lg-0">-->
<!--                    <img src="images/about_1.jpg" alt="Image " class="img-fluid img-overlap">-->
<!--                </div>-->
<!--                <div class="col-lg-5 ml-auto">-->
<!--                    <h3 class="section-subtitle">Why Choose Us</h3>-->
<!--                    <h2 class="section-title mb-4">More than <strong>50 year experience</strong> in industry</h2>-->
<!--                    <p>Reprehenderit, odio laboriosam? Blanditiis quae ullam quasi illum minima nostrum perspiciatis error consequatur sit nulla.</p>-->
<!---->
<!--                    <div class="row my-5">-->
<!--                        <div class="col-lg-12 d-flex align-items-center mb-4">-->
<!--                            <span class="line-height-0 flaticon-oil-platform display-4 mr-4 text-primary"></span>-->
<!--                            <div>-->
<!--                                <h4 class="m-0 h5 text-white">Expert in Builings</h4>-->
<!--                                <p class="text-white">Lorem ipsum dolor sit amet.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-12 d-flex align-items-center mb-4">-->
<!--                            <span class="line-height-0 flaticon-compass display-4 mr-4 text-primary"></span>-->
<!--                            <div>-->
<!--                                <h4 class="m-0 h5 text-white">Modern Design</h4>-->
<!--                                <p class="text-white">Lorem ipsum dolor sit amet.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-12 d-flex align-items-center">-->
<!--                            <span class="line-height-0 flaticon-planning display-4 mr-4 text-primary"></span>-->
<!--                            <div>-->
<!--                                <h4 class="m-0 h5 text-white">Leading In Floor Planning</h4>-->
<!--                                <p class="text-white">Lorem ipsum dolor sit amet.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!---->
<!---->
<!--                    </div>-->
<!---->
<!---->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--END block-2-->
<!---->
<!---->
<!--<div class="">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-5 align-self-end">-->
<!--                <img src="images/img_transparent.png" alt="Image" class="img-fluid">-->
<!--            </div>-->
<!--            <div class="col-lg-7 align-self-center mb-5">-->
<!---->
<!--                <div class="bg-black  quote-form-wrap wrap text-white">-->
<!--                    <div class="mb-5">-->
<!--                        <h3 class="section-subtitle">Get A Quote</h3>-->
<!--                        <h2 class="section-title mb-4">Request A <strong>Quote</strong></h2>-->
<!--                    </div>-->
<!--                    <form action="#" class="quote-form">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-6 form-group">-->
<!--                                <input type="text" class="form-control" placeholder="Your name*">-->
<!--                            </div>-->
<!--                            <div class="col-md-6 form-group">-->
<!--                                <input type="text" class="form-control" placeholder="Phone number">-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="row">-->
<!--                            <div class="col-md-6 form-group">-->
<!--                                <input type="text" class="form-control" placeholder="Your email*">-->
<!--                            </div>-->
<!--                            <div class="col-md-6 form-group">-->
<!--                                <input type="text" class="form-control" placeholder="Subject">-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="row">-->
<!--                            <div class="col-md-6">-->
<!--                                <textarea name="" class="form-control" id="" placeholder="Message*" cols="30" rows="7"></textarea>-->
<!--                            </div>-->
<!--                            <div class="col-md-6 align-self-end">-->
<!--                                <input type="submit" class="btn btn-primary btn-block btn-lg rounded-0" value="Send Message">-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!---->
<!---->
<!--<div class="site-section block-3">-->
<!--    <div class="container">-->
<!---->
<!--        <div class="mb-5">-->
<!--            <h3 class="section-subtitle">Our Projects</h3>-->
<!--            <h2 class="section-title mb-4">Explore Our <strong>Recent Projects</strong></h2>-->
<!--        </div>-->
<!---->
<!--        <div class="projects-carousel-wrap">-->
<!--            <div class="owl-carousel owl-slide-3">-->
<!--                <div class="project-item">-->
<!--                    <div class="project-item-contents">-->
<!--                        <a href="#">-->
<!--                            <span class="project-item-category">Factory</span>-->
<!--                            <h2 class="project-item-title">-->
<!--                                Building Refinery-->
<!--                            </h2>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <img src="images/works_1.jpg" alt="Image" class="img-fluid">-->
<!--                </div>-->
<!--                <div class="project-item">-->
<!--                    <div class="project-item-contents">-->
<!--                        <a href="#">-->
<!--                            <span class="project-item-category">Factory</span>-->
<!--                            <h2 class="project-item-title">-->
<!--                                Building Refinery-->
<!--                            </h2>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <img src="images/works_2.jpg" alt="Image" class="img-fluid">-->
<!--                </div>-->
<!--                <div class="project-item">-->
<!--                    <div class="project-item-contents">-->
<!--                        <a href="#">-->
<!--                            <span class="project-item-category">Factory</span>-->
<!--                            <h2 class="project-item-title">-->
<!--                                Building Refinery-->
<!--                            </h2>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <img src="images/works_3.jpg" alt="Image" class="img-fluid">-->
<!--                </div>-->
<!--                <div class="project-item">-->
<!--                    <div class="project-item-contents">-->
<!--                        <a href="#">-->
<!--                            <span class="project-item-category">Factory</span>-->
<!--                            <h2 class="project-item-title">-->
<!--                                Building Refinery-->
<!--                            </h2>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <img src="images/works_4.jpg" alt="Image" class="img-fluid">-->
<!--                </div>-->
<!---->
<!---->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div>-->
<!--</div>-->
<!---->
<!---->
<!---->
<!--<div class="site-section testimonial-wrap">-->
<!--    <div class="container">-->
<!--        <div class="row justify-content-center">-->
<!--            <div class="col-12 mb-5 text-center">-->
<!--                <h3 class="section-subtitle">Testimonial</h3>-->
<!--                <h2 class="section-title text-black mb-4">What People Says</h2>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="row">-->
<!--            <div class="col-md-6 mb-4 mb-md-0">-->
<!--                <div class="testimonial">-->
<!--                    <img src="images/person_3_sq.jpg" alt="">-->
<!--                    <blockquote>-->
<!--                        <p>&ldquo;Lorem ipsum dolor sit, amet consectetur adipisicing elit. Provident deleniti iusto molestias, dolore vel fugiat ab placeat ea?&rdquo;</p>-->
<!--                    </blockquote>-->
<!--                    <p class="client-name">Matt Keygen</p>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 mb-4 mb-md-0">-->
<!--                <div class="testimonial">-->
<!--                    <img src="images/person_4_sq.jpg" alt="">-->
<!--                    <blockquote>-->
<!--                        <p>&ldquo;Lorem ipsum dolor sit, amet consectetur adipisicing elit. Provident deleniti iusto molestias, dolore vel fugiat ab placeat ea?&rdquo;</p>-->
<!--                    </blockquote>-->
<!--                    <p class="client-name">Matt Keygen</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!---->
<!---->
<!--<div class="site-section bg-light">-->
<!--    <div class="container">-->
<!--        <div class="row justify-content-center">-->
<!--            <div class="col-12 mb-5 text-left">-->
<!--                <h3 class="section-subtitle">Blog</h3>-->
<!--                <h2 class="section-title text-black mb-4">News &amp; Updates</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col-md-6 mb-4 mb-lg-0 col-lg-4">-->
<!--                <div class="blog-entry">-->
<!--                    <a href="#" class="img-link">-->
<!--                        <img src="images/hero_1.jpg" alt="Image" class="img-fluid">-->
<!--                    </a>-->
<!--                    <div class="blog-entry-contents">-->
<!--                        <h3><a href="#">Top Companies That Are Best In Industrial Business</a></h3>-->
<!--                        <div class="meta">Posted by <a href="#">Admin</a> In <a href="#">News</a></div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 mb-4 mb-lg-0 col-lg-4">-->
<!--                <div class="blog-entry">-->
<!--                    <a href="#" class="img-link">-->
<!--                        <img src="images/hero_1.jpg" alt="Image" class="img-fluid">-->
<!--                    </a>-->
<!--                    <div class="blog-entry-contents">-->
<!--                        <h3><a href="#">Top Companies That Are Best In Industrial Business</a></h3>-->
<!--                        <div class="meta">Posted by <a href="#">Admin</a> In <a href="#">News</a></div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-6 mb-4 mb-lg-0 col-lg-4">-->
<!--                <div class="blog-entry">-->
<!--                    <a href="#" class="img-link">-->
<!--                        <img src="images/hero_1.jpg" alt="Image" class="img-fluid">-->
<!--                    </a>-->
<!--                    <div class="blog-entry-contents">-->
<!--                        <h3><a href="#">Top Companies That Are Best In Industrial Business</a></h3>-->
<!--                        <div class="meta">Posted by <a href="#">Admin</a> In <a href="#">News</a></div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!---->
<!--<div class="py-5 bg-primary block-4">-->
<!--    <div class="container">-->
<!--        <div class="row align-items-center">-->
<!--            <div class="col-lg-6">-->
<!--                <h3 class="text-white">Subscribe To Newsletter</h3>-->
<!--                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nesciunt, reprehenderit!</p>-->
<!--            </div>-->
<!--            <div class="col-lg-6">-->
<!--                <form action="#" class="form-subscribe d-flex">-->
<!--                    <input type="text" class="form-control form-control-lg">-->
<!--                    <input type="submit" class="btn btn-secondary px-4" value="Subcribe">-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->