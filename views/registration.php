<div class="site-section testimonial-wrap">
    <div class="container">
        <form method="post" >
            <div class="form-group">
                <label for="exampleInputLogin">Login</label>
                <input type="text" class="form-control" id="exampleInputLogin" aria-describedby="emailHelp" name="login">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password">
            </div>
            <!--    <div class="form-group form-check">-->
            <!--        <input type="checkbox" class="form-check-input" id="exampleCheck1">-->
            <!--        <label class="form-check-label" for="exampleCheck1">Check me out</label>-->
            <!--    </div>-->
            <button type="submit" class="btn btn-primary">Regisration</button>
        </form>
    </div>
</div>
