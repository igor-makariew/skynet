<?php
    require_once ('core/controller.php');
    require_once ('models/site.model.php');
    require_once ('models/task.model.php');

    class Site extends Controller{
        public $site_model;
        public $task_model;
        public $core;

        function __construct($core){
            parent::__construct($core);

            $this->core = $core;
            $this->site_model = new SiteModel();
            $this->task_model = new TaskModel();


            if(isset($_SESSION['user']['login'])){
                $this->set_private($_SESSION['user']['status']);
            }else{
                $this->set_private(GUEST);
            }

            $this->configuration->add_css('css/bootstrap.min.css');
            $this->configuration->add_css('css/jquery-ui.css');
            $this->configuration->add_css('css/owl.theme.default.min.css');
            $this->configuration->add_css('css/owl.theme.default.min.css');
            $this->configuration->add_css('css/owl.carousel.min.css');
            $this->configuration->add_css('css/jquery.fancybox.min.css');
            $this->configuration->add_css('css/bootstrap-datepicker.css');
            $this->configuration->add_css('css/aos.css');
            $this->configuration->add_css('css/jquery.mb.YTPlayer.min.css');
            $this->configuration->add_css('css/style.css');

            $this->configuration->add_js('js/jquery-3.3.1.min.js');
            $this->configuration->add_js('js/jquery-migrate-3.0.1.min.js');
            $this->configuration->add_js('js/jquery-ui.js');
            $this->configuration->add_js('js/popper.min.js');
            $this->configuration->add_js('js/bootstrap.min.js');
            $this->configuration->add_js('js/owl.carousel.min.js');
            $this->configuration->add_js('js/jquery.stellar.min.js');
            $this->configuration->add_js('js/jquery.countdown.min.js');
            $this->configuration->add_js('js/bootstrap-datepicker.min.js');
            $this->configuration->add_js('js/jquery.easing.1.3.js');
            $this->configuration->add_js('js/aos.js');
            $this->configuration->add_js('js/jquery.fancybox.min.js');
            $this->configuration->add_js('js/jquery.sticky.js');
            $this->configuration->add_js('js/jquery.mb.YTPlayer.min.js');
            $this->configuration->add_js('js/main.js');

            $this->configuration->add_fonts('https://fonts.googleapis.com/css?family=Oswald:300,400,700|Muli:300,400');
            $this->configuration->add_fonts('fonts/icomoon/style.css');
            $this->configuration->add_fonts('fonts/flaticon/font/flaticon.css');
        }


        function actionIndex(){
            $this->params['title'] = 'Главная';

            //запись данных по задачам от пользователя
            $login = trim(filter_input(INPUT_POST, 'login'));
            $email = trim(filter_input(INPUT_POST, 'email'));
            $status = trim(filter_input(INPUT_POST, 'status'));
            $text_task = trim(filter_input(INPUT_POST, 'text_task'));

            if(!empty($login) && !empty($email) && !empty($status) && !empty($text_task)){
                $this->task_model->set_task_user($login, $email, $status, $text_task);
            }

            //пагинация
            if(!isset($_GET['page'])){
                $_GET['page'] = 1;
            }
            $_SESSION['page'] = $_GET['page'];
            $this->core->get_pagination()->set_page($_GET['page']);
            $this->core->get_pagination()->set_on_page(3);
            $this->core->get_pagination()->set_shift($this->task_model->get_tasks(), $this->core->get_pagination()->get_page(), $this->core->get_pagination()->get_on_page());
            $this->core->get_pagination()->set_count($this->task_model->get_count_tasks());
            $this->core->get_pagination()->set_count_pages(ceil($this->core->get_pagination()->get_count()[0]['count(*)'] / $this->core->get_pagination()->get_on_page()));
            $this->params['pages'] = $this->core->get_pagination()->get_count_pages();

            //выборка списка задач
            if($this->task_model->array_params_sort($this->site_model->get_url_params($_SERVER['REQUEST_URI']))){
                $get_url_value = $this->site_model->get_url_value($this->site_model->get_url_params($_SERVER['REQUEST_URI']));
                $get_order_by_column = $this->site_model->get_order_by_column($this->site_model->get_url_params($_SERVER['REQUEST_URI']));
                $this->core->get_pagination()->set_shift($this->site_model->sort_tasks($get_order_by_column, $get_url_value), $this->core->get_pagination()->get_page(), $this->core->get_pagination()->get_on_page());
                $this->params['table'] = $this->core->get_pagination()->get_shift();
            }else{
                $this->params['table'] = $this->core->get_pagination()->get_shift();
            }

            return $this->render('views/index.php');
        }

        function actionLogin(){
            $this->params['title'] = 'Login';

            // получение данных от пользователя
            $login = filter_input(INPUT_POST, 'login');
            $password = filter_input(INPUT_POST, 'password');

            if(!empty($login) && !empty($password)){
                if($this->site_model->verify_pasword_user($login, $password)){$this->site_model->select_user($login);
                    $_SESSION['user'] = $this->site_model->select_user($login);
                    $new_url = 'http://skynet/?action=admin/indexadmin';
                    header('Location: '.$new_url);
                } else{
                    $_SESSION['user'] = false;
                    $new_url = 'http://skynet/?action=site/login';
                    header('Location: '.$new_url);
                }
            }

            return $this->render('views/login.php');
        }

        function actionRegistration(){
            $this->params[''] = 'Registration';
            //получение данных от пользователя
            $login = trim(filter_input(INPUT_POST, 'login'));
            $email = trim(filter_input(INPUT_POST, 'email'));
            $password = trim(filter_input(INPUT_POST, 'password'));

            if(!empty($login) && !empty($email) && !empty($password)){
                if($this->site_model->select_email_user($email)){
                    if($this->site_model->registration_user($login, $email, $password, USER)){
                        $new_url = 'http://skynet/?action=site/login';
                        header('Location: '.$new_url);
                    }
                }else{
                    $new_url = 'http://skynet/';
                    header('Location: '.$new_url);
                }
            }

            return $this->render('views/registration.php');
        }
    }








