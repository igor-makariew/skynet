<?php
    class Url{
        public $controller_name;
        public $action_name;

       function __construct($action){
           $url_piece = explode('/', $action);
           if($url_piece[0] != ''){
               $this->controller_name = $url_piece[0];
           }
           if($url_piece[1] != ''){
               $this->action_name = $url_piece[1];
           }
       }

       function get_controller_name(){
           return $this->controller_name;
       }

       function get_action_name(){
           return $this->action_name;
       }

        function is_this_current_page($address){
            if ($address == '/') {
                if ($this->controller_name == "") {
                    if ($this->action_name == "index") return true;
                }
            }

            $pieces = explode("/", $address);
            if (count($pieces) == 1) {
                if ($this->controller_name == "") {
                    if ($this->action_name == $pieces[0]) return true;
                }
            } elseif (count($pieces) == 2) {
                if ($this->controller_name == $pieces[0] && $this->action_name == $pieces[1]) return true;
            }
            return false;
        }
    }
