<?php
    require_once ('core/default.php');

    class Model{
        public $user = 'admin';
        public $localhost = 'localhost';
        public $password = 'admin';
        public $database = 'skynet';
        public $link;

        public function __construct(){
            $this->link = mysqli_connect($this->host, $this->user, $this->password, $this->database);
        }

        function get_link(){
            return $this->link;
        }

    }
