<?php
    require_once ('controllers/site.controller.php');
    require_once ('url.php');
    require_once ('core/pagination.php');
    require_once ('controllers/admin.controller.php');

    class Core{
        public $url;
        public $active_controller;
        public $name_controller;
        public $database;
        public $pagination;

        public function __construct(){
            //Запускаем сессию
            if(!isset($_SESSION)) {
                session_start();
            }

            $this->database = new Model();
            $this->pagination = new Pagination($this);

        }

        function get_url(){
            return $this->url;
        }

        function get_pagination(){
            return $this->pagination;
        }

        function start() {
            $this->active_controller = new Site($this);
            $action = filter_input(INPUT_GET, 'action');
            $this->url = new Url($action);

            if($this->url->get_controller_name() != ''){
                if(class_exists($this->url->get_controller_name())){
                   $this->name_controller = ucfirst($this->url->get_controller_name());
                   $this->active_controller = new $this->name_controller($this);
                }
            }

            if($this->url->get_action_name() != ''){
                if(method_exists($this->active_controller, 'action'.$this->url->get_action_name())){
                    $this->active_controller->{'action'.ucfirst($this->url->get_action_name())}();
                }else{
                    $this->active_controller->{'action404'};
                }
            }else{
                $this->active_controller->actionIndex();
            }

        }

    }
