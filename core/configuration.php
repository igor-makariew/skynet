<?php
    class Configuration{
        public $array_css = array();
        public $array_js = array();
        public $array_fonts = array();
        public $layout = '';

        function add_css($css){
            array_push($this->array_css, $css);
        }

        function get_css(){
            return $this->array_css;
        }

        function add_js($js){
            array_push($this->array_js, $js);
        }

        function get_js(){
            return $this->array_js;
        }

        function add_fonts($fonts){
            array_push($this->array_fonts, $fonts);
        }

        function get_fonts(){
            return $this->array_fonts;
        }

        function set_layout($layout){
            $this->layout = $layout;
        }

        function get_layout(){
            return $this->layout;
        }
    }
