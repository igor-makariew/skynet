<?php
    require_once ('configuration.php');
    require_once ('url.php');

    class Controller{
        public $configuration;
        public $params;
        public $role;

        public function __construct(){
            $this->configuration = new Configuration();
        }

        function create_css_style(){
            $content = '';
            foreach($this->configuration->get_css() as $item){
                $content .= "<link href='{$item}' type='text/css' rel='stylesheet'>";
            }
            return $content;
        }

        function create_js_script(){
            $content = '';
            foreach($this->configuration->get_js() as $item){
                $content .= "<script src='{$item}'></script>";
            }
            return $content;
        }

        function create_fonts(){
            $content = '';
            foreach($this->configuration->get_fonts() as $item){
                $content .= "<link href='{$item}' type='text/css' rel='stylesheet'>";
            }
            return $content;
        }

        function render($view){
            // задаем путь шаблона
            $this->configuration->set_layout('./layout/engeener/layout_index.php');
            $params = $this->params;
            ob_start();
            include $view;
            $content = ob_get_clean();
            $title = $this->params['title'];

            include $this->configuration->get_layout();
        }

        function set_private($role){
            $this->role = $role;
        }

        function  get_private(){
            return $this->role;
        }
    }
