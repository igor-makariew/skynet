<?php
    class Pagination{
        public $page;
        public $on_page;
        public $shift;
        public $count;
        public $count_pages;

        function set_page($page){
            $this->page = $page;
        }

        function get_page(){
            return $this->page;
        }

        function set_on_page($on_page){
            $this->on_page = $on_page;
        }

        function get_on_page(){
            return $this->on_page;
        }

        function set_shift($array, $offset, $length){
            $this->shift = array_slice($array, $length*($offset-1), $length*$offset);
        }

        function get_shift(){
            return $this->shift;
        }

        function set_count($count){
            $this->count = $count;
        }

        function get_count(){
            return $this->count;
        }

        function set_count_pages($count_pages){
            $this->count_pages = $count_pages;
        }

        function get_count_pages(){
            return $this->count_pages;
        }
    }
