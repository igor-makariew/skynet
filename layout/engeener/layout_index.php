<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?= $this->create_css_style()?>
    <?= $this->create_fonts()?>

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

<div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>



    <div class="header-top bg-light">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6 col-lg-3">
                    <a href="index.html">
                        <img src="images/logo.png" alt="Image" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-3 d-none d-lg-block">

                    <div class="quick-contact-icons d-flex">
                        <div class="icon align-self-start">
                            <span class="flaticon-placeholder text-primary"></span>
                        </div>
                        <div class="text">
                            <span class="h4 d-block">San Francisco</span>
                            <span class="caption-text">Mountain View, Fake st., CA</span>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 d-none d-lg-block">
                    <div class="quick-contact-icons d-flex">
                        <div class="icon align-self-start">
                            <span class="flaticon-call text-primary"></span>
                        </div>
                        <div class="text">
                            <span class="h4 d-block">000 209 392 312</span>
                            <span class="caption-text">Toll free</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 d-none d-lg-block">
                    <div class="quick-contact-icons d-flex">
                        <div class="icon align-self-start">
                            <span class="flaticon-email text-primary"></span>
                        </div>
                        <div class="text">
                            <span class="h4 d-block">info@gmail.com</span>
                            <span class="caption-text">Gournadi, 1230 Bariasl</span>
                        </div>
                    </div>
                </div>


                <div class="col-6 d-block d-lg-none text-right">
                    <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                            class="icon-menu h3"></span></a>
                </div>
            </div>

            </div>
        </div>

        <div class="site-navbar py-2 js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner">

            <div class="container">
                <div class="d-flex align-items-center">

                    <div class="mr-auto">
                        <nav class="site-navigation position-relative text-right" role="navigation">
                            <ul class="site-menu main-menu js-clone-nav mr-auto d-none pl-0 d-lg-block">
                                <li <?= $this->core->get_url()->is_this_current_page('/')?"class='active'":"" ?> >
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="/"  class="nav-link text-left">Home</a>
                                    <?php else:?>
                                        <a href="/?action=admin/indexadmin"  class="nav-link text-left">Home</a>
                                    <?php endif;?>
                                </li>
                                <li>
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="about.html" class="nav-link text-left">About Us</a>
                                    <?php endif;?>
                                </li>
                                <li>
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="works.html" class="nav-link text-left">Our Projects</a>
                                    <?php endif;?>
                                </li>
                                <li>
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="testimonials.html" class="nav-link text-left">Testimonials</a>
                                    <?php endif;?>
                                </li>
                                <li>
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="blog.html" class="nav-link text-left">Blog</a></li>
                                    <?php endif;?>
                                <li>
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="contact.html" class="nav-link text-left">Contact</a>
                                    <?php endif;?>
                                </li>
                                <li <?= $this->core->get_url()->is_this_current_page('site/login')?"class='active'":"" ?> >
                                    <?php if(!isset($_SESSION['user']['login'])):?>
                                        <a href="/?action=site/login" class="nav-link text-leftt">Login</a>
                                    <?php endif;?>
                                </li>
                                <?php if(isset($_SESSION['user']['login'])):?>
                                    <li <?= $this->core->get_url()->is_this_current_page('site/exit')?"class='active'":"" ?>>
                                        <a href="/?action=admin/exitadmin" class="nav-link text-left">You - <?= $_SESSION['user']['login'] ?></a>
                                    </li>
                                <?php endif;?>
                            </ul>                                                                                                                                                                                                                                                                                          </ul>
                        </nav>

                    </div>

                </div>
            </div>

        </div>

    </div>

    <div class="hero-slide owl-carousel site-blocks-cover">
        <div class="intro-section" style="background-image: url('images/hero_1.jpg');">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
                        <h1>We Are <strong>Leading</strong> Industry of Engineers</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="intro-section" style="background-image: url('images/hero_1.jpg');">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
                        <span class="d-block"></span>
                        <h1>Experts and High Quality Works</h1>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END slider -->

    <?= $content; ?>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <p class="mb-4"><img src="images/logo2.png" alt="Image" class="img-fluid"></p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae nemo minima qui dolor, iusto iure.</p>
                    <p><a href="#">Learn More</a></p>
                </div>
                <div class="col-lg-3">
                    <h3 class="footer-heading"><span>Our Company</span></h3>
                    <ul class="list-unstyled">
                        <li><a href="#">About</a></li>
                        <li><a href="#">News</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Our Team</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Projects</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <h3 class="footer-heading"><span>Our Services</span></h3>
                    <ul class="list-unstyled">
                        <li><a href="#">Industrial</a></li>
                        <li><a href="#">Construction</a></li>
                        <li><a href="#">Remodeling</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <h3 class="footer-heading"><span>Contact</span></h3>
                    <ul class="list-unstyled">
                        <li><a href="#">Help Center</a></li>
                        <li><a href="#">Support Community</a></li>
                        <li><a href="#">Press</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Our Partners</a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="copyright">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- .site-wrap -->


<!-- loader -->
<!--<div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#ff5e15"/></svg></div>-->
<!---->
<?= $this->create_js_script() ?>

</body>

</html>